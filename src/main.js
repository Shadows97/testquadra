import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './firebase'
import {rtdbPlugin} from 'vuefire'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
import {VueCoolSelect} from 'vue-cool-select'


Vue.use(Buefy);
Vue.use(rtdbPlugin);
Vue.use(VueCoolSelect, {
  theme: 'bootstrap' // or 'material-design'
});
Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
