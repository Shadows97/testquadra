import firebase from 'firebase/app'
import 'firebase/database'

//initialisation des parametre de connexion a la base de donne firebase
const  app = firebase.initializeApp({ apiKey: "AIzaSyBR-WuSg5FUCYMUk545rsmgOZqHAqysozA",
    authDomain: "testproject-ba69b.firebaseapp.com",
    databaseURL: "https://testproject-ba69b.firebaseio.com",
    projectId: "testproject-ba69b",
    storageBucket: "",
    messagingSenderId: "86891807038",
    appId: "1:86891807038:web:e4c80be5e8879c982c2c6f"});

export const db = app.database();
export const dataRef = db.ref('categories');
